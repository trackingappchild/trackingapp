package com.bv.teensafechild;

import android.content.Context;
import android.util.Log;

import com.bv.teensafechild.bean.CallLogs;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.service.Call_Manager;
import com.bv.teensafechild.util.Utils;

import java.util.List;

/**
 * Created by Administrator on 9/28/2016.
 */

public class CallReceiver extends Call_Manager{
    CallLogs log=new CallLogs();
    DatabaseHandler db ;

    @Override
    protected void onIncomingCallReceived(Context ctx, String number, String start) {
        Log.d("System out", "onIncomingCallReceived :: "+number+" ---- "+start);
    }

    @Override
    protected void onIncomingCallAnswered(Context ctx, String number, String start) {
        Log.d("System out", "onIncomingCallAnswered :: "+number+" ---- "+start);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, String start, String end) {

        db = new DatabaseHandler(ctx);
        Log.d("System out", "onIncomingCallEnded :: "+number+" ---- "+start+" ::End:: "+end);
        long startTime = Utils.getSeconds("yyyy-MM-dd HH:mm:ss", start);
        long endTime = Utils.getSeconds("yyyy-MM-dd HH:mm:ss", end);
        String duration=Utils.timeFormat((int) (endTime-startTime));
        Log.d("System out", "Time duration:::: "+ Utils.timeFormat((int) (endTime-startTime)));

        // DB entry put here for incoming
        String type="INCOMING";
        log=new CallLogs();
        log.setcallnumber(number);
        log.setcalltype(type);
        log.setcalldaytime(String.valueOf(start));
        log.setcallduration(duration);

       /*Inserting values in database*/
        Log.d("Insert: ", "Inserting log ..");
        db.addlog(log);
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, String start) {
        Log.d("System out", "onOutgoingCallStarted :: "+number+" ---- "+start);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, String start, String end) {
        db = new DatabaseHandler(ctx);
        Log.d("System out", "onOutgoingCallEnded :: "+number+" ---- "+start+" ::End:: "+end);

        // DB entry put here for outgoing
        long startTime = Utils.getSeconds("yyyy-MM-dd HH:mm:ss", start);
        long endTime = Utils.getSeconds("yyyy-MM-dd HH:mm:ss", end);
        String duration=Utils.timeFormat((int) (endTime-startTime));
        Log.d("System out", "Time duration:::: "+ Utils.timeFormat((int) (endTime-startTime)));

        // DB entry put here for incoming
        String type="OUTGOING";
        log=new CallLogs();
        log.setcallnumber(number);
        log.setcalltype(type);
        log.setcalldaytime(String.valueOf(start));
        log.setcallduration(duration);

       /*Inserting values in database*/
        Log.d("Insert: ", "Inserting log ..");
        db.addlog(log);
    }

    @Override
    protected void onMissedCall(Context ctx, String number, String start) {
        db = new DatabaseHandler(ctx);
        Log.d("System out", "onMissedCall :: "+number+" ---- "+start);
        // DB entry put here for missed call
        long startTime = Utils.getSeconds("yyyy-MM-dd HH:mm:ss", start);
        String type="MISSED";
        log=new CallLogs();
        log.setcallnumber(number);
        log.setcalltype(type);
        log.setcalldaytime(String.valueOf(start));
        log.setcallduration("");

       /*Inserting values in database*/
        Log.d("Insert: ", "Inserting log ..");
        db.addlog(log);
    }
}
