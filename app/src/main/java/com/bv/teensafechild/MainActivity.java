package com.bv.teensafechild;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.bv.teensafechild.bean.AppInstall;
import com.bv.teensafechild.bean.CallLogs;
import com.bv.teensafechild.bean.Contact;
import com.bv.teensafechild.bean.History;
import com.bv.teensafechild.bean.LocationBean;
import com.bv.teensafechild.bean.SMSLogBean;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.database.LocContentProvider;
import com.bv.teensafechild.service.AlarmReceiver;
import com.bv.teensafechild.service.MessageLogging;
import com.bv.teensafechild.service.PackageChangeReceiverCall;
import com.bv.teensafechild.service.SMS_SentService;
import com.bv.teensafechild.service.UpdateLocation;
import com.bv.teensafechild.util.Log;
import com.bv.teensafechild.util.Pref;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Switch browserSwitch, mSwitch_gps, contactSwitch, logSwitch,appSwitch, mSwitch_msg, mSwitch_All;
    private Button btnSave, btnCancel;
    private boolean mIsAll = false;
    private boolean mIsBrowser = false;
    private boolean mIsMessage = false;
    private boolean mIsContact = false;
    private boolean mIsTrackLoc = false;
    private boolean mIsLog = false;
    private boolean mIsAPPInstall = false;
    private static final String HISTORY_INTENT_ACTION = "com.bv.teensafechild.BrowserActivity";
    private static final String HISTORY_INTENT_ACTION_CONTACT = "com.bv.teensafechild.ContactActivity";


    private boolean detectEnabled;

    private PendingIntent tracking;
    private AlarmManager alarms;
    //SimpleCursorAdapter adapter;

    private long UPDATE_INTERVAL = 10000;
    private int START_DELAY = 5;
    private String DEBUG_TAG = "System out";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initViews();
        initListeners();

        getBrowserDataForTest();
        getCallLogDataForTest();
        getContactDataForTest();
        getLocation();
        getAppDataForTest();
        getSMS();


    }

    public void getBrowserDataForTest(){
        DatabaseHandler db = new DatabaseHandler(this);
        List<History> histroylist=db.getAllhistory();
        Log.print("System out", "Browser Data:::: "+histroylist.size());
    }

    public void getLocation(){
        DatabaseHandler db = new DatabaseHandler(MyApp.getContext());
        List<LocationBean> locationBeanList = db.getLocationData();
        Log.print("System out", "Location Data:::: "+locationBeanList.size());
    }

    public void getCallLogDataForTest(){
        DatabaseHandler db = new DatabaseHandler(this);
        List<CallLogs> histroylist=db.getAllLogs();
        Log.print("System out", "Call Data:::: "+histroylist.size());
    }
    public void getContactDataForTest(){
        DatabaseHandler db = new DatabaseHandler(this);
        List<Contact> histroylist=db.getAllContact();
        Log.print("System out", "Contact Data:::: "+histroylist.size());
    }
    public void getAppDataForTest(){
        DatabaseHandler db = new DatabaseHandler(this);
        List<AppInstall> applist=db.getAllApps();
        Log.print("System out", "App Data:::: "+applist.size());
    }

    public void getSMS(){
        DatabaseHandler db = new DatabaseHandler(MyApp.getContext());
        List<SMSLogBean> smsData= db.getSMSData();
        Log.print("System out", "SMS Data:::: " + smsData.size());
    }

    protected void initViews() {

        browserSwitch = (Switch) findViewById(R.id.switch_browser);
        mSwitch_gps = (Switch)findViewById(R.id.switch_gps);
        contactSwitch = (Switch) findViewById(R.id.switch_cont);
        logSwitch = (Switch) findViewById(R.id.switch_calllogs);
        mSwitch_msg = (Switch) findViewById(R.id.switch_msg);
        appSwitch=(Switch) findViewById(R.id.switch_appInstall);
        mSwitch_All = (Switch)findViewById(R.id.switch_all);

        btnSave = (Button) findViewById(R.id.btn_save);

        if (Pref.getValue(this, Pref.KEY_ALL, false)){
            mIsAll = true;
            mSwitch_All.setChecked(true);
            setEnableAllServices();
        }else{
            mIsAll = false;
            mSwitch_All.setChecked(false);
            setDisableAllServices();
        }

        if (Pref.getValue(this, Pref.KEY_STATUS_BROWSER, false)) {
            mIsBrowser = true;
            browserSwitch.setChecked(true);

        } else {
            mIsBrowser=false;
            browserSwitch.setChecked(false);
        }

        if (Pref.getValue(this, Pref.KEY_STATUS_CONTACT, false)) {
            mIsContact=true;
            contactSwitch.setChecked(true);
        } else {
             mIsContact=false;
             contactSwitch.setChecked(false);
        }

        //Check Prefs for GPS Location:-
        if (Pref.getValue(this, Pref.KEY_STATUS_GPSLOC, false)) {
            mIsTrackLoc=true;
            mSwitch_gps.setChecked(true);
        } else {
            mIsTrackLoc=false;
            mSwitch_gps.setChecked(false);
        }

        if (Pref.getValue(this, Pref.KEY_STATUS_LOG, false)) {
            mIsLog = true;
            logSwitch.setChecked(true);

        } else {
            mIsLog = false;
            logSwitch.setChecked(false);
        }

        if (Pref.getValue(this, Pref.KEY_STATUS_MSGLOG, false)) {
            mIsMessage = true;
            mSwitch_msg.setChecked(true);

        } else {
            mIsMessage=false;
            mSwitch_msg.setChecked(false);
        }

        if (Pref.getValue(this, Pref.KEY_STATUS_APP, false)) {
            mIsAPPInstall = true;
            appSwitch.setChecked(true);

        } else {
            mIsAPPInstall = false;
            appSwitch.setChecked(false);
        }

    }

    public void initListeners() {
        //set the switch to ON

        mSwitch_All.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsAll = true;
                    setEnableAllServices();
                } else {
                    mIsAll = false;
                    setDisableAllServices();
                }
            }
        });

        browserSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsBrowser = true;
                    System.out.print("buttonclickservice>>>");
                    //startService(new Intent(MainActivity.this, BrowserServiceCall.class));
                } else {
                    mIsBrowser = false;
                }
            }
        });

        contactSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsContact = true;
                    System.out.print("buttoncontactclickservice>>>");
                } else {
                    mIsContact = false;
                }
            }

        });

        mSwitch_gps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsTrackLoc = true;
                } else {
                    mIsTrackLoc = false;
                }
            }

        });

        mSwitch_msg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsMessage = true;
                } else {
                    mIsMessage = false;
                }
            }

        });

        logSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsLog = true;
                    System.out.print("buttonlogclickservice>>>");
                } else {
                    mIsLog = false;
                }
            }

        });
        appSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsAPPInstall = true;
                    System.out.print("buttonAppclickservice>>>");
                } else {
                    mIsAPPInstall = false;
                }
            }

        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*for browser service*/

                if (mIsAll){
                    Pref.setValue(MainActivity.this, Pref.KEY_ALL, true);
                }else{
                    Pref.setValue(MainActivity.this, Pref.KEY_ALL, false);
                }

                if (mIsBrowser) {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_BROWSER, true);
                    Intent broadcastIntent = new Intent(HISTORY_INTENT_ACTION);
                    broadcastIntent.putExtra("start", true);
                    sendBroadcast(broadcastIntent);

                } else {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_BROWSER, false);
                    Intent broadcastIntent = new Intent(HISTORY_INTENT_ACTION);
                    broadcastIntent.putExtra("start", false);
                    sendBroadcast(broadcastIntent);
                }

                 /*for contact service*/
                if (mIsContact) {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_CONTACT, true);
                    Intent broadcastIntent = new Intent(HISTORY_INTENT_ACTION_CONTACT);
                    broadcastIntent.putExtra("start", true);
                    sendBroadcast(broadcastIntent);

                } else {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_CONTACT, false);
                    Intent broadcastIntent = new Intent(HISTORY_INTENT_ACTION_CONTACT);
                    broadcastIntent.putExtra("start", false);
                    sendBroadcast(broadcastIntent);
                }

                if (mIsTrackLoc) {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_GPSLOC, true);
                    alarms = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
                    setRecurringAlarm(getBaseContext());

                } else {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_GPSLOC, false);

                    /*Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                    tracking = PendingIntent.getBroadcast(getBaseContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                    alarms = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
                    alarms.cancel(tracking);

                    stopService(new Intent(MainActivity.this, UpdateLocation.class));*/

                    Log.print(DEBUG_TAG, ">>>Stop tracking()");
                    Intent i = new Intent(MainActivity.this, UpdateLocation.class);
                    PendingIntent pintent = PendingIntent.getService(MainActivity.this, 0, i, 0);
                    AlarmManager alarm = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
                    alarm.cancel(pintent);

                    /*Intent broadcastIntent = new Intent(HISTORY_INTENT_ACTION);
                    broadcastIntent.putExtra("start", false);
                    sendBroadcast(broadcastIntent);*/

                }

                /*Message Logging:-*/
                if (mIsMessage) {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_MSGLOG, true);
                    enableSMSBroadcastReceiver(v);
                } else {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_MSGLOG, false);
                    disableSMSBroadcastReceiver(v);
                }


                 /*for contact service*/
                if (mIsLog) {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_LOG, true);
                   enableBroadcastReceiver(v);

                } else {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_LOG, false);
                    disableBroadcastReceiver(v);

                }

                  /*for app service*/
                if (mIsAPPInstall) {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_APP, true);
                    enableAppBroadcastReceiver(v);

                } else {
                    Pref.setValue(MainActivity.this, Pref.KEY_STATUS_APP, false);
                    disableAppBroadcastReceiver(v);

                }
            }
        });

       /* btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pref.setValue(MainActivity.this, Pref.KEY_STATUS, false);
                Intent broadcastIntent = new Intent(HISTORY_INTENT_ACTION);
                broadcastIntent.putExtra("start", false);
                sendBroadcast(broadcastIntent);
            }
        });*/
    }

    public void setEnableAllServices(){
        mIsBrowser = true;
        browserSwitch.setChecked(true);

        mIsContact=true;
        contactSwitch.setChecked(true);

        mIsTrackLoc=true;
        mSwitch_gps.setChecked(true);

        mIsLog = true;
        logSwitch.setChecked(true);

        mIsMessage = true;
        mSwitch_msg.setChecked(true);

        mIsAPPInstall = true;
        appSwitch.setChecked(true);
    }
    public void setDisableAllServices(){
        mIsBrowser = false;
        browserSwitch.setChecked(false);

        mIsContact=false;
        contactSwitch.setChecked(false);

        mIsTrackLoc=false;
        mSwitch_gps.setChecked(false);

        mIsLog = false;
        logSwitch.setChecked(false);

        mIsMessage = false;
        mSwitch_msg.setChecked(false);

        mIsAPPInstall = false;
        appSwitch.setChecked(false);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } /*else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setRecurringAlarm(Context context) {
        // get a Calendar object with current time
        Calendar cal = Calendar.getInstance();
        // add 5 minutes to the calendar object
        cal.add(Calendar.SECOND, START_DELAY);
        Intent intent = new Intent(context, AlarmReceiver.class);
        tracking = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), UPDATE_INTERVAL, tracking);
    }

    @Override
    public void onResume() {
        super.onResume();
        //drawTable();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    // Creates a new loader after the initLoader () call

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = { DatabaseHandler.COLUMN_ID, DatabaseHandler.COLUMN_TIME,
                DatabaseHandler.COLUMN_LONGITUDE, DatabaseHandler.COLUMN_LATITUDE };
        CursorLoader cursorLoader = new CursorLoader(this,
                LocContentProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }


    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        //adapter.swapCursor(data);
    }


    public void onLoaderReset(Loader<Cursor> loader) {
        // data is not available anymore, delete reference
        //adapter.swapCursor(null);
    }


    public void enableSMSBroadcastReceiver(View view)
    {
        ComponentName receiver = new ComponentName(this, MessageLogging.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        startService(new Intent(getApplicationContext(), SMS_SentService.class));

    }
    /**
     * This method disables the Broadcast receiver registered in the AndroidManifest file.
     * @param view
     */
    public void disableSMSBroadcastReceiver(View view){
        ComponentName receiver = new ComponentName(this, MessageLogging.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        stopService(new Intent(getApplicationContext(), SMS_SentService.class));
//        Toast.makeText(this, "Disabled broadcst receiver", Toast.LENGTH_SHORT).show();
    }


    public void enableBroadcastReceiver(View view)
    {

        ComponentName receiver = new ComponentName(this, CallReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


    //    Toast.makeText(this, "Enabled broadcast receiver", Toast.LENGTH_SHORT).show();
    }
    /**
     * This method disables the Broadcast receiver registered in the AndroidManifest file.
     * @param view
     */
    public void disableBroadcastReceiver(View view){
        ComponentName receiver = new ComponentName(this, CallReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
      //  Toast.makeText(this, "Disabled broadcst receiver", Toast.LENGTH_SHORT).show();
    }


    public void enableAppBroadcastReceiver(View view)
    {

        ComponentName receiver = new ComponentName(this, PackageChangeReceiverCall.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
       // Toast.makeText(this, "Enabled App broadcast receiver", Toast.LENGTH_SHORT).show();
    }
    /**
     * This method disables the Broadcast receiver registered in the AndroidManifest file.
     * @param view
     */
    public void disableAppBroadcastReceiver(View view){
        ComponentName receiver = new ComponentName(this, PackageChangeReceiverCall.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
       // Toast.makeText(this, "Disabled App broadcast receiver", Toast.LENGTH_SHORT).show();
    }

}