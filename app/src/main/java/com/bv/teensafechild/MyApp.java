package com.bv.teensafechild;

import android.app.Application;
import android.content.Context;

/**
 * Created by Administrator on 9/19/2016.
 */
public class MyApp extends Application {
    //private static MyApp instance;
    private static Context mContext;

    public static Context getContext() {
        //  return instance.getApplicationContext();
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //  instance = this;
        mContext = getApplicationContext();
    }
}
