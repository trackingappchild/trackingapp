package com.bv.teensafechild.bean;

/**
 * Created by shivani.bajpai on 9/26/2016.
 */
public class AppInstall {

    //private variables
    int id;
    public String packName;
    public String action;
    public String appName;
    public String date;

    // Empty constructor
    public AppInstall(){

    }
    // constructor
    public AppInstall(int id, String packName, String action ,String appName,String date){
        this.id = id;
        this.packName=packName;
        this.action=action;
        this.appName=appName;
        this.date=date;

    }

    // constructor
    public AppInstall(String packName, String action ,String appName,String date){
        this.id = id;
        this.packName=packName;
        this.action=action;
        this.appName=appName;
        this.date=date;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
