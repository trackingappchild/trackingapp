package com.bv.teensafechild.bean;

/**
 * Created by shivani.bajpai on 8/30/2016.
 */
public class CallLogs {

    //private variables
    int _id;
    public String _call_number;
    public String _call_type;
    public String _call_daytime;
    public String _call_duration;

    // Empty constructor
    public CallLogs(){

    }
    // constructor
    public CallLogs(int id, String call_number, String call_type , String call_daytime, String call_duration){
        this._id = id;
        this._call_number=call_number;
        this._call_type=call_type;
        this._call_daytime=call_daytime;
        this._call_duration=call_duration;

    }

    // constructor
    public CallLogs(String call_number, String call_type, String call_daytime, String call_duration){
        this._call_number=call_number;
        this._call_type=call_type;
        this._call_daytime=call_daytime;
        this._call_duration=call_duration;
    }

    public int getid() {
        return _id;
    }

    public void setid(int _id) {
        this._id = _id;
    }

    public String getcallnumber() {
        return _call_number;
    }

    public void setcallnumber(String _call_number) {
        this._call_number = _call_number;
    }

    public String getcalltype() {
        return _call_type;
    }

    public void setcalltype(String _call_type) {
        this._call_type = _call_type;
    }

    public String getcalldaytime() {
        return _call_daytime;
    }

    public void setcalldaytime(String _call_daytime) {
        this._call_daytime = _call_daytime;
    }

    public String getcallduration() {
        return _call_duration;
    }

    public void setcallduration(String _call_duration) {
        this._call_duration = _call_duration;
    }
}
