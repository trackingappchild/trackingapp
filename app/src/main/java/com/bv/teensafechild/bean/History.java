package com.bv.teensafechild.bean;

/**
 * Created by shivani.bajpai on 9/13/2016.
 */
public class History {

    int id;
    public String title;
    public String url;
    public String currentTimeStamp;

    // Empty constructor
    public History(){

    }
    // constructor
    public History(int id, String title, String url, String date){
        this.id = id;
        this.title=title;
        this.url=url;
        this.currentTimeStamp=date;
          }

    // constructor
    public History(String title, String url, String date){
        this.title=title;
        this.url=url;
        this.currentTimeStamp=date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public void setCurrentTimeStamp(String currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }
}
