package com.bv.teensafechild.bean;

/**
 * Created by shivani.bajpai on 8/30/2016.
 */
public class LocationBean {

    //private variables
    String _id;
    String time;
    String longitude;
    String latitude;

    // Empty constructor
    public LocationBean() {
    }

    // constructor
    public LocationBean(String time, String longitude, String latitude) {
        this.time = time;
        this.longitude = longitude;
        this.latitude = latitude;
    }

  /*  // constructor
    public Location(String name, String _phone_number, String _email){
        this._name = name;
        this._phone_number = _phone_number;
        this._email=_email;
    }*/

    // getting ID
    public String getID() {
        return this._id;
    }

    // setting id
    public void setID(String _id) {
        this._id = _id;
    }


    // getting ID
    public String getTime() {
        return this.time;
    }

    // setting id
    public void setTime(String time) {
        this.time = time;
    }


    // getting ID
    public String getLongitude() {
        return this.longitude;
    }

    // setting id
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    // getting ID
    public String getLatitude() {
        return this.latitude;
    }

    // setting id
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

}
