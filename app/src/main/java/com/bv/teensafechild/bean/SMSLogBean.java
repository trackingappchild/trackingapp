package com.bv.teensafechild.bean;

/**
 * Created by shivani.bajpai on 8/30/2016.
 */
public class SMSLogBean {

    //private variables
    int _id;
    public String _sms_number;
    public String _sms_type;
    public String _sms_date;
    public String _sms_content;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_sms_number() {
        return _sms_number;
    }

    public void set_sms_number(String _sms_number) {
        this._sms_number = _sms_number;
    }

    public String get_sms_type() {
        return _sms_type;
    }

    public void set_sms_type(String _sms_type) {
        this._sms_type = _sms_type;
    }

    public String get_sms_date() {
        return _sms_date;
    }

    public void set_sms_date(String _sms_date) {
        this._sms_date = _sms_date;
    }

    public String get_sms_content() {
        return _sms_content;
    }

    public void set_sms_content(String _sms_content) {
        this._sms_content = _sms_content;
    }
}

