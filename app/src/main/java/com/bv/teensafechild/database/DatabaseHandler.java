package com.bv.teensafechild.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bv.teensafechild.bean.AppInstall;
import com.bv.teensafechild.bean.CallLogs;
import com.bv.teensafechild.bean.Contact;
import com.bv.teensafechild.bean.History;
import com.bv.teensafechild.bean.LocationBean;
import com.bv.teensafechild.bean.SMSLogBean;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by shivani.bajpai on 8/30/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";

    // Contacts table name
    private static final String TABLE_LOG = "log";


    // SMS Message table name
    private static final String TABLE_SMS = "sms";


    // Contacts table name
    private static final String TABLE_HISTORY = "history";

    // app table name
    private static final String TABLE_APP = "app";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";
    private static final String KEY_EMAIL = "email";

    private static final String KEY_CALL_NO = "call_number";
    private static final String KEY_CALL_TYPE = "call_type";
    private static final String KEY_CALL_DATE = "call_date";
    private static final String KEY_CALL_DAYTIME = "call_daytime";
    private static final String KEY_CALL_DURATION = "call_duration";


    private static final String KEY_SMS_NO = "sms_number";
    private static final String KEY_SMS_TYPE = "sms_type";//sent or recieve
    private static final String KEY_SMS_DATE = "sms_date";
    //private static final String KEY_CALL_DAYTIME = "call_daytime";
    private static final String KEY_SMS_CONTENT = "sms_content";


    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_URL = "url";
    private static final String COLUMN_DATE = "date";

    private static final String COLUMN_NEW_ID = "id";
    private static final String COLUMN_PACKAGE_NAME = "name";
    private static final String COLUMN_ACTION_TAKEN = "action";
    private static final String COLUMN_APP_NAME= "appname";
    private static final String COLUMN_APP_DATE= "date";


    // Database table
    public static final String TABLE_TODO = "loc";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_LATITUDE = "latitude";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT," + KEY_EMAIL + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

        String CREATE_LOG_TABLE = "CREATE TABLE " + TABLE_LOG + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + KEY_CALL_NO + " TEXT,"
                + KEY_CALL_TYPE + " TEXT," + KEY_CALL_DAYTIME + " TEXT," + KEY_CALL_DURATION + " TEXT" + ")";

        db.execSQL(CREATE_LOG_TABLE);

        /*SMS Logging Table:- */
        String CREATE_SMS_TABLE = "CREATE TABLE " + TABLE_SMS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + KEY_SMS_NO + " TEXT,"
                + KEY_SMS_TYPE + " TEXT," + KEY_SMS_DATE + " TEXT," + KEY_SMS_CONTENT + " TEXT" + ")";
        db.execSQL(CREATE_SMS_TABLE);


        // Database creation SQL statement
        String DATABASE_CREATE = "CREATE TABLE "
                + TABLE_TODO
                + " ("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + COLUMN_TIME + " TEXT NOT NULL, "
                + COLUMN_LONGITUDE + " TEXT NOT NULL, "
                + COLUMN_LATITUDE + " TEXT NOT NULL "
                + ")";

        db.execSQL(DATABASE_CREATE);

        // Database creation SQL statement
        String CREATE_HISTORY_TABLE = "CREATE TABLE "
                + TABLE_HISTORY
                + " ("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + COLUMN_TITLE + " TEXT NOT NULL, "
                + COLUMN_URL + " TEXT NOT NULL, "
                + COLUMN_DATE + " TEXT NOT NULL "
                + ")";

        db.execSQL(CREATE_HISTORY_TABLE);

        // Database creation SQL statement
        String CREATE_APP_TABLE = "CREATE TABLE "
                + TABLE_APP
                + " ("
                + COLUMN_NEW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + COLUMN_PACKAGE_NAME + " TEXT NOT NULL, "
                + COLUMN_ACTION_TAKEN + " TEXT NOT NULL, "
                + COLUMN_APP_NAME + " TEXT NOT NULL, "
                + COLUMN_APP_DATE + " TEXT NOT NULL "
                + ")";

        db.execSQL(CREATE_APP_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName()); // Contact Name
        values.put(KEY_PH_NO, contact.getPhoneNumber());
        values.put(KEY_EMAIL, contact.getEmail());// Contact Phone

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);

        db.close(); // Closing database connection
    }

  /*  // Adding new contact
    void addlog(CallLog log) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CALL_NO, log._call_number); // Contact Name
        values.put(KEY_CALL_DATE, log._call_date);
        values.put(KEY_CALL_DAYTIME, log._call_daytime);
        values.put(KEY_CALL_DURATION, log._call_duration);
        values.put(KEY_CALL_TYPE, log._call_type);

        // Inserting Row
        db.insert(TABLE_LOG, null, values);
        db.close(); // Closing database connection
    }*/


    // Adding new contact
    public void addhistory(History hs) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, hs.title); // Contact Name
        values.put(COLUMN_URL, hs.url);
        values.put(COLUMN_DATE, hs.currentTimeStamp);
        // Inserting Row
        db.insert(TABLE_HISTORY, null, values);
        // db.close(); // Closing database connection
    }

    // Adding new contact
    public void addlog(CallLogs log) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CALL_NO, log._call_number); // Contact Name
        values.put(KEY_CALL_DAYTIME, log._call_daytime);
        values.put(KEY_CALL_DURATION, log._call_duration);
        values.put(KEY_CALL_TYPE, log._call_type);

        // Inserting Row
        db.insert(TABLE_LOG, null, values);
        db.close(); // Closing database connection
    }

    // Getting single contact
    public Contact getContact() {
        SQLiteDatabase db = this.getReadableDatabase();

      /*  Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);*/
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " " + "ORDER BY" + " " + KEY_ID + " " + "DESC Limit" + " " + 1;
        SQLiteDatabase contactdb = this.getWritableDatabase();
        Cursor cursor = contactdb.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToFirst();

        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
        // return contact
        db.close();
        return contact;
    }
    // Getting all contact
    public List<Contact> getAllContact() {
        List<Contact> contList = new ArrayList<Contact>();
        SQLiteDatabase db = this.getReadableDatabase();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " " + "ORDER BY" + " " + KEY_ID;
        SQLiteDatabase contactdb = this.getWritableDatabase();
        Cursor cursor = contactdb.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), cursor.getString(3));
                // Adding contact to list
                contList.add(contact);
            } while (cursor.moveToNext());
        }
        // return contact
        db.close();
        return contList;
    }


    // Getting single contact
    public History Gethistory() {

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_HISTORY + " " + "ORDER BY" + " " + COLUMN_DATE + " " + "DESC Limit" + " " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToFirst();

        History history = new History(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));

        db.close();

        return history;
    }
    // Getting single call log
    public CallLogs GetCallLog() {
        CallLogs log = null;

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_LOG +" "+ "ORDER BY" +" "+ KEY_CALL_DAYTIME + " " + "DESC Limit"+" "+1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null)
            cursor.moveToFirst();

        log = new CallLogs(cursor.getString(1), cursor.getString(2), cursor.getString(4), cursor.getString(5));

        db.close();
        return log;
    }
    // Getting single contact
    public List<History> getAllhistory() {
        List<History> historyList = new ArrayList<History>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_HISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                History history= new History();
                history.setId(Integer.parseInt(cursor.getString(0)));
                history.setTitle(cursor.getString(1));
                history.setUrl(cursor.getString(2));
                history.setCurrentTimeStamp(cursor.getString(3));
                // Adding contact to list
                historyList.add(history);
            } while (cursor.moveToNext());
        }

        db.close();
        // return contact list
        return historyList;
    }

    // Getting All Call Logs
    public List<CallLogs> getAllLogs() {
        List<CallLogs> LogList = new ArrayList<CallLogs>();
        // Select All Query
        // String selectQuery = "SELECT  * FROM " + TABLE_LOG;
        String selectQuery = "SELECT * FROM " + TABLE_LOG +" "+ "ORDER BY" +" "+ KEY_CALL_DAYTIME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CallLogs log=new CallLogs();
                log.setid(Integer.parseInt(cursor.getString(0)));
                log.setcallnumber(cursor.getString(1));
                log.setcalltype(cursor.getString(2));
                log.setcalldaytime(cursor.getString(3));
                log.setcallduration(cursor.getString(4));
                // Adding contact to list
                LogList.add(log);
            } while (cursor.moveToNext());
        }
        db.close();
        // return contact list
        return LogList;
    }

    public boolean checkUrlExist(String url){
        SQLiteDatabase db = this.getWritableDatabase();
        boolean mIsExist=false;
        if (db!=null){
            if (!db.isOpen()){
                db = this.getWritableDatabase();
            }
            Cursor rawQuery = db.rawQuery("SELECT * FROM " + TABLE_HISTORY + " WHERE "+COLUMN_URL+" = '" + url + "'", null);
            if (rawQuery.getCount() > 0) { // This will get the number of rows
                // It has data
                mIsExist = true;
            }
            db.close();
        }

        return mIsExist;
    }



    public boolean checkContactExist(String name,String phoneNumber){
        SQLiteDatabase db = this.getWritableDatabase();
        boolean mIsExist=false;
        Cursor rawQuery = db.rawQuery("SELECT * FROM " + TABLE_CONTACTS + " WHERE "+KEY_NAME+" = '" + name+"'"+ " "+ "AND"+" "+KEY_PH_NO+" "+" = '" + phoneNumber +"'", null);
        if (rawQuery.getCount() > 0) { // This will get the number of rows
            // It has data
            mIsExist = true;
        }
        return mIsExist;
    }


    public void dropTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        // Create tables again
        onCreate(db);
    }

    // Updating single contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PH_NO, contact.getPhoneNumber());

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(contact.getID())});
    }

    // Deleting single contact
    public void deleteContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[]{String.valueOf(contact.getID())});
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Getting Locations data
    public List<LocationBean> getLocationData() {
        List<LocationBean> appList = new ArrayList<LocationBean>();
        String countQuery = "SELECT  * FROM " + TABLE_TODO;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LocationBean app = new LocationBean(cursor.getString(1), cursor.getString(2),cursor.getString(3));
                // Adding app to list
                appList.add(app);
            } while (cursor.moveToNext());
        }
        db.close();
        return appList;
    }
    // Adding new app install
    public void addApp(AppInstall appInstall) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_PACKAGE_NAME, appInstall.getPackName()); // app Name
        values.put(COLUMN_ACTION_TAKEN, appInstall.getAction());
        values.put(COLUMN_APP_NAME,appInstall.getAppName());
        values.put(COLUMN_APP_DATE,appInstall.getDate());

        // Inserting Row
        db.insert(TABLE_APP, null, values);

        db.close(); // Closing database connection
    }
    // Getting single call log
    public List<AppInstall> getAllApps() {
        List<AppInstall> appList = new ArrayList<AppInstall>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_APP +" "+ "ORDER BY" +" "+ COLUMN_APP_DATE + " " + "DESC ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AppInstall app = new AppInstall(cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4));
                // Adding app to list
                appList.add(app);
            } while (cursor.moveToNext());
        }
        db.close();

        return appList;
    }


    // Adding new contact
    public void addLocation(LocationBean location) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TIME, location.getTime());
        values.put(COLUMN_LATITUDE, location.getLatitude());
        values.put(COLUMN_LONGITUDE, location.getLongitude());

        db.insert(TABLE_TODO, null, values);
        db.close();

        //result = "Location: " + Double.toString(longitude)+", "+Double.toString(latitude);
    }


    // Adding new contact
    public void addsms(SMSLogBean SMSlog) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SMS_NO, SMSlog._sms_number); // Contact Name
        values.put(KEY_SMS_DATE,SMSlog._sms_date);
        values.put(KEY_SMS_CONTENT, SMSlog.get_sms_content());
        values.put(KEY_SMS_TYPE, SMSlog._sms_type);

        // Inserting Row
        db.insert(TABLE_SMS, null, values);
        db.close(); // Closing database connection
    }

    // Getting SMS data
    public List<SMSLogBean>  getSMSData() {

        List<SMSLogBean> LogList = new ArrayList<SMSLogBean>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SMS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SMSLogBean log=new SMSLogBean();
                log.set_id(Integer.parseInt(cursor.getString(0)));
                log.set_sms_number(cursor.getString(1));
                log.set_sms_type(cursor.getString(2));
                log.set_sms_date(cursor.getString(3));
                log.set_sms_content(cursor.getString(4));
                // Adding contact to list
                LogList.add(log);
            } while (cursor.moveToNext());
        }
        //cursor.close();
        db.close();
        // return SMS list
        return LogList;


    }

}