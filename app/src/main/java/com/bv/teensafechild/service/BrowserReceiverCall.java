package com.bv.teensafechild.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by shivani.bajpai on 9/13/2016.
 */
public class BrowserReceiverCall extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("receivercall>>", "browser");

        boolean flag=intent.getBooleanExtra("start",false);

        if(flag) {

            Calendar cal = new GregorianCalendar();
            cal.set(Calendar.SECOND, 30);

            Intent i = new Intent(context, BrowserServiceCall.class);
            Log.e("receivercall>>", "Time:: "+cal.getTime());

            PendingIntent pintent = PendingIntent.getService(context, 0, i, 0);
            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 10 * 1000, pintent);
        }else
        {
            Intent i = new Intent(context, BrowserServiceCall.class);
            PendingIntent pintent = PendingIntent.getService(context, 0, i, 0);
            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarm.cancel(pintent);

        }

       // context.startService(new Intent(context, BrowserServiceCall.class));;
    }

}

