package com.bv.teensafechild.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

import com.bv.teensafechild.MyApp;
import com.bv.teensafechild.R;
import com.bv.teensafechild.bean.History;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.util.BookmarkColumns;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by shivani.bajpai on 9/13/2016.
 */
public class BrowserServiceCall extends Service {

    private ContentResolver cr;
    History history;
    DatabaseHandler db = new DatabaseHandler(MyApp.getContext());
    public static final Uri BOOKMARKS_URI = Uri.parse("content://browser/bookmarks");

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        createLists();
        history = new History();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {

           /*method for fetching and inserting values */
            Thread t = new Thread(){
                public void run(){
                    getHistory();
                    System.out.print("onStartCommand call>>>");
                }
            };
            t.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    public void onDestroy() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent("com.bv.teensafechild.service.service");
        intent.putExtra("yourvalue", "torestore");
        sendBroadcast(intent);


    }

    /*public void createLists() {
        titles = new ArrayList<String>();
        urls = new ArrayList<String>();
        bitmaps = new ArrayList<Bitmap>();
        StrDate = new ArrayList<String>();

    }*/


    public void getBrowserHistory() {
        Bitmap icon;
        cr = getContentResolver();
//        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
        String order = BookmarkColumns.DATE + " DESC";
        String[] projection = {BookmarkColumns.TITLE, BookmarkColumns.URL, BookmarkColumns.FAVICON, BookmarkColumns.DATE};
        String sel = BookmarkColumns.BOOKMARK + " = 0"; // 0 = history, 1 = bookmark
        Cursor rows = getContentResolver().query(BOOKMARKS_URI, projection,
                sel, null, order);

            if (rows.getCount() > 0) {
                rows.moveToNext();

                //read title
                String title = rows.getString(rows.getColumnIndex(projection[0]));
                //read url
                String url = rows.getString(rows.getColumnIndex(projection[1]));
                //read icon
                byte[] bicon = rows.getBlob(rows.getColumnIndex(projection[2]));
                if (bicon != null) {
                    //convert blob image data to Bitmap
                    icon = BitmapFactory.decodeByteArray(bicon, 0, bicon.length);

                } else {
                    //default icon for history and bookmarks that do not icons
                    icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                }
                //Get browser url

                String strDate = rows.getString(rows.getColumnIndex(projection[3]));
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Long.parseLong(strDate));
                SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
                System.out.println(calendar.getTime());
                // Output "Wed Sep 26 14:23:28 EST 2012"

                String currenttime = format1.format(calendar.getTime());

                if (db.checkUrlExist(url)){
                    System.out.print("Already exist in Default Browser>>>>>");
                }else{
                    /*Inserting values */
                    history = new History();
                    history.setTitle(title);
                    history.setUrl(url);
                    history.setCurrentTimeStamp(currenttime);

                    db.addhistory(history);
                    System.out.print("inserting values Default Browser>>>>>");
                }
            }

            //close the cursor
            rows.close();
//        } else {
//            Toast.makeText(this, "you cannot check browser history for new version", Toast.LENGTH_LONG).show();
//        }
    }

    public void getChromeHistory() {
        Bitmap icon;
        cr = getContentResolver();
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            String order = BookmarkColumns.DATE + " DESC";
            String[] projection = {BookmarkColumns.TITLE, BookmarkColumns.URL, BookmarkColumns.FAVICON, BookmarkColumns.DATE};
            Uri uriCustom = Uri.parse("content://com.android.chrome.browser/bookmarks");
            String sel = BookmarkColumns.BOOKMARK + " = 0"; // 0 = history, 1 = bookmark

            Cursor rows = getContentResolver().query(uriCustom, projection, null, null, order);

            if (rows.getCount() > 0) {
                rows.moveToNext();
                String title = rows.getString(rows.getColumnIndex(projection[0]));
                //read url
                String url = rows.getString(rows.getColumnIndex(projection[1]));
                //read icon
                byte[] bicon = rows.getBlob(rows.getColumnIndex(projection[2]));
                if (bicon != null) {
                    //convert blob image data to Bitmap
                    icon = BitmapFactory.decodeByteArray(bicon, 0, bicon.length);

                } else {
                    //default icon for history and bookmarks that do not icons
                    icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
                }

                //read date and time from browser
                String strDate = rows.getString(rows.getColumnIndex(projection[3]));
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Long.parseLong(strDate));
                SimpleDateFormat format1 = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
                System.out.println(calendar.getTime());
                // Output "Wed Sep 26 14:23:28 EST 2012"

                String currenttime = format1.format(calendar.getTime());

                if (db.checkUrlExist(url)){
                    System.out.print("Already exist in Chrome>>>>>");
                }else{
                    /*Inserting values */
                    history = new History();
                    history.setTitle(title);
                    history.setUrl(url);
                    history.setCurrentTimeStamp(currenttime);

                    db.addhistory(history);
                    System.out.print("inserting values Chrome>>>>>");
                }
            }
            //close the cursor
            rows.close();
        } else {
            handler.sendEmptyMessage(0);
        }


    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Toast.makeText(getApplicationContext(), "you cannot check browser history for new version", Toast.LENGTH_LONG).show();
        }
    };


    /*database queries  */
    public void getHistory() {
//        db.dropTable();
        getBrowserHistory();
        getChromeHistory();

        /*fetching values*/
        /*History hs = db.Gethistory();
        System.out.print("fetching values>>>>");
        String title = hs.getTitle();
        String url = hs.getUrl();
        String date = hs.getCurrentTimeStamp();*/

    }


}

