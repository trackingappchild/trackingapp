package com.bv.teensafechild.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;

import com.bv.teensafechild.bean.Contact;
import com.bv.teensafechild.database.DatabaseHandler;

import java.util.List;

;

/**
 * Created by shivani.bajpai on 9/19/2016.
 */
public class ContactServiceCall extends Service {

    DatabaseHandler db = new DatabaseHandler(this);
    List<Contact> contacts;
    Contact contact;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {

           /*method for fetching and inserting values */
            insertContacts();
            fetchContacts();


        } catch (Exception e) {
            e.printStackTrace();
        }
        //return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    public void onDestroy() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent("com.bv.teensafechild.service.service");
        intent.putExtra("yourvalue", "torestore");
        sendBroadcast(intent);


    }


    public void insertContacts() {

        contact = new Contact();

        String phoneNumber = null;
        String email = null;
        String name = null;
        String contact_id = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
       // String CURRENT_TIMESTAMP=ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;


        StringBuffer output = new StringBuffer();
        String order = ContactsContract.Contacts._ID + " DESC";

        ContentResolver contentResolver = getContentResolver();

       /* Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);*/

        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                ContactsContract.Contacts.HAS_PHONE_NUMBER + " = 1",
                null, order);

        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            contact_id = cursor.getString(cursor.getColumnIndex(_ID));
            name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
            int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
              if (hasPhoneNumber > 0) {

                output.append("\n First Name:" + name);

                // Query and loop for every phone number of the contact
                Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);

                while (phoneCursor.moveToNext()) {
                    phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                    output.append("\n Phone number:" + phoneNumber);


                }

                phoneCursor.close();

                // Query and loop for every email of the contact
                Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);

                while (emailCursor.moveToNext()) {

                    email = emailCursor.getString(emailCursor.getColumnIndex(DATA));

                    output.append("\nEmail:" + email);

                }

               emailCursor.close();
            }

            output.append("\n");
            if (db.checkContactExist(name,phoneNumber)) {
                System.out.print("Already exist in Contact>>>>>");
            } else {
                contact.setID(Integer.parseInt(contact_id));
                contact.setName(name);
                contact.setPhoneNumber(phoneNumber);
                contact.setEmail(email);

            /*Inserting values in database*/
                Log.d("Insert: ", "Inserting ..");
                db.addContact(contact);
                contacts=db.getAllContact();

                // outputText.setText(output);
                cursor.close();
            }
        }

    }

    public void fetchContacts() {
         /*get single contact*/
        contact = db.getContact();
        String name = contact.getName();
        String number = contact.getPhoneNumber();
        String email = contact.getEmail();
        System.out.print("onStartCommand fetch contact>>>");

    }


}


