package com.bv.teensafechild.service;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.bv.teensafechild.bean.SMSLogBean;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.util.Utils;

public class MessageLogging extends BroadcastReceiver {

    Context mContext;
    DatabaseHandler db;
    private String origin;
    private String body;
    private String mType;
    private String mDate;

    BroadcastReceiver mReceiver;



    @Override
    public void onReceive(Context mContext, Intent intent){
        this.mContext = mContext;

        Bundle extras = intent.getExtras();
        Object[] pdus = (Object[]) extras.get("pdus");

        Log.i("::SENT::", "OnReceive");

        for (Object pdu : pdus) {

            Log.i("::FOR::", "OnReceive");

            SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu);
            origin = msg.getOriginatingAddress();
            System.out.println("::Sender::-" + origin);
            body = msg.getMessageBody();
            System.out.println("::Message Content::-" + body);
            //Get Current Date:-
            mDate = Utils.getCurrentDate();
            System.out.println("Current Date Time : " + mDate);

        }
        int duration = Toast.LENGTH_LONG;
        Toast toast = null;
        toast = Toast.makeText(mContext, intent.getAction(), duration);
        toast.show();
        if (intent.getAction()
                .equals("android.provider.Telephony.SMS_RECEIVED")) {
            mType = "RECEIVED";
            System.out.println("::intent.getAction():-RCV-" + intent.getAction());
            System.out.println("--:RCV:--android.provider.Telephony.SMS_RECEIVED:--");
            toast = Toast.makeText(mContext, "recebeu", duration);
            toast.show();
        } else if (intent.getAction().equals(
                "android.provider.Telephony.SMS_SENT")) {
            mType = "SENT";
            System.out.println("--:SENT:--android.provider.Telephony.SMS_SENT:--");
            toast = Toast.makeText(mContext, "enviou", duration);
            toast.show();
        }

        if (intent.getAction() != null) {
            SMSLogBean mSMSLogBean = new SMSLogBean();
            mSMSLogBean.set_sms_number(origin);
            mSMSLogBean.set_sms_date(mDate);
            mSMSLogBean.set_sms_type(mType);
            mSMSLogBean.set_sms_content(body);

            db = new DatabaseHandler(mContext);
            db.addsms(mSMSLogBean);
        }

    }

    /*private String trackLocation(Location location) {
        double longitude;
        double latitude;
        String time;
        String result = "Location currently unavailable.";

        // Insert a new record into the Events data source.
        // You would do something similar for delete and update.
        if (location != null)
        {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            time = parseTime(location.getTime());
            ContentValues values = new ContentValues();

            LocationBean mLocationBean = new LocationBean();
            String mLat =  String.valueOf(latitude);
            String mLng =  String.valueOf(longitude);

            mLocationBean.setLatitude(mLat);
            mLocationBean.setLongitude(mLng);
            mLocationBean.setTime(time);

            db.addLocation(mLocationBean);
            result = "Location: " + Double.toString(longitude)+", "+Double.toString(latitude);
        }
        return result;
    }*/



}