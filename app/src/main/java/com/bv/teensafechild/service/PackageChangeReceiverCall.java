package com.bv.teensafechild.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.bv.teensafechild.bean.AppInstall;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.util.Log;
import com.bv.teensafechild.util.Pref;
import com.bv.teensafechild.util.Utils;

import java.util.StringTokenizer;

/**
 * Created by shivani.bajpai on 9/26/2016.
 */
public class PackageChangeReceiverCall extends BroadcastReceiver {

    private static final String TAG = "package>>>";
    AppInstall appInstall;
    DatabaseHandler db;
    String applicationName;

    @Override
    public void onReceive(Context ctx, Intent intent) {
        appInstall = new AppInstall();
        db = new DatabaseHandler(ctx);
        Uri data = intent.getData();
        String uridata = data.toString();
        boolean replacing = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);

        StringTokenizer tokens = new StringTokenizer(uridata, ":");
        String first = tokens.nextToken();// this will contain "package"
        String second = tokens.nextToken();

        applicationName = getApplicationName(ctx, second, 0);
        String date = Utils.getCurrentDate();

        Log.print(TAG, "Action: " + intent.getAction());
        Log.print(TAG, "The DATA: " + data);
        Log.print(TAG, "Replacing is: " + replacing);
        Log.print(TAG, "Name is: " + applicationName);
        Log.print(TAG, "Date is: " + date);


///
        if (Pref.getValue(ctx, Pref.KEY_STATUS_APP, false)) {
            appInstall.setPackName(uridata);
            if(intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                appInstall.setAction("INSTALL");
            }else if(intent.getAction().equals("android.intent.action.PACKAGE_REMOVED"))
            {
                appInstall.setAction("UNINSTALL");
            }else{
                appInstall.setAction("UPDATE");
            }
            appInstall.setAppName(applicationName);
            appInstall.setDate(date);
            db.addApp(appInstall);

        }

    }

    private String getApplicationName(Context context, String data, int flag) {

        final PackageManager pckManager = context.getPackageManager();
        ApplicationInfo applicationInformation;
        try {
            applicationInformation = pckManager.getApplicationInfo(data, flag);


        } catch (PackageManager.NameNotFoundException e) {
            applicationInformation = null;
        }
        final String applicationName = (String) (applicationInformation != null ? pckManager.getApplicationLabel(applicationInformation) : data);
        return applicationName;

    }
}
