package com.bv.teensafechild.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;

import com.bv.teensafechild.custom.SMSObserver;
import com.bv.teensafechild.util.Log;

public class SMS_SentService extends Service {
    public SMS_SentService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.print("System out","----SMS----onStartCommand----");
        SMSObserver myObserver = new SMSObserver(new Handler(), this);
        ContentResolver contentResolver = this.getApplicationContext().getContentResolver();
        contentResolver.registerContentObserver(Uri.parse("content://mms-sms"), true, myObserver);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        Log.print("System out","----SMS----onBind----");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.print("System out","----SMS----onDestroy----");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.print("System out","----SMS----onUnbind----");
        return super.onUnbind(intent);
    }
}
