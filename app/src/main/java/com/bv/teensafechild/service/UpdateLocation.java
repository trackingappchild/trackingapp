package com.bv.teensafechild.service;

import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.bv.teensafechild.bean.LocationBean;
import com.bv.teensafechild.database.DatabaseHandler;
import com.bv.teensafechild.util.Pref;
import com.bv.teensafechild.util.Utils;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class UpdateLocation extends Service implements LocationListener {

	DatabaseHandler db;

	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;
	private final String DEBUG_TAG = "System out";
	private LocationManager mgr;
	private String best;


	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {

			Location location = mgr.getLastKnownLocation(best);
			mServiceHandler.post(new MakeToast(trackLocation(location)));
			// Stop the service using the startId, so that we don't stop
			// the service in the middle of handling another job
			stopSelf(msg.arg1);
		}
	}

	@Override
	public void onCreate() {
		// Start up the thread running the service.  Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block.  We also make it
		// background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread("ServiceStartArguments",
				android.os.Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		Log.d(DEBUG_TAG, ">>>onCreate()");
		// Get the HandlerThread's Looper and use it for our Handler
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		mgr = (LocationManager) getSystemService(LOCATION_SERVICE);

		Criteria criteria = new Criteria();
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(false);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		best = mgr.getBestProvider(criteria, true);

		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String providerFine = mgr.getBestProvider(criteria, true);

		criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		String providerCoarse = mgr.getBestProvider(criteria, true);



		if (providerCoarse != null) {
			mgr.requestLocationUpdates(providerCoarse, 30000, 5, this);
		}
		if (providerFine != null) {
			mgr.requestLocationUpdates(providerFine, 2000, 0, this);
		}else{
			mgr.requestLocationUpdates(best, 15000, 1, this);
		}

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
//      Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);
		Log.d(DEBUG_TAG, ">>>onStartCommand()");
		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		return null;
	}

	@Override
	public void onDestroy() {
//    Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
		Log.d(DEBUG_TAG, ">>>onDestroy()");
	}

	//obtain current location, insert into database and make toast notification on screen
	private String trackLocation(Location location) {
		double longitude;
		double latitude;
		String time;
		String result = "Location not updated.";

		// Insert a new record into the Events data source.
		// You would do something similar for delete and update.
		if (location != null)
		{
			longitude = location.getLongitude();
			latitude = location.getLatitude();

			LatLng latLng = new LatLng(latitude, longitude);

			float distance = Utils.getDistance(latLng, Pref.getValue(getApplicationContext(), "prev_loc"));

			if (distance>10){
				db = new DatabaseHandler(this);
				result = "Location: " + Double.toString(latLng.latitude)+", "+Double.toString(latLng.longitude);
				Pref.setValue(getApplicationContext(), "prev_loc", latLng);
				time = Utils.getCurrentDate();

				LocationBean mLocationBean = new LocationBean();

				mLocationBean.setLatitude(""+latLng.latitude);
				mLocationBean.setLongitude(""+latLng.longitude);
				mLocationBean.setTime(time);
				db.addLocation(mLocationBean);
			}else{
				com.bv.teensafechild.util.Log.print("System out", "No update Location::::  "+result);
			}
		}
		return result;
	}


	private class MakeToast implements Runnable {
		String txt;
		public MakeToast(String text){
			txt = text;
		}
		public void run(){

			com.bv.teensafechild.util.Log.print("System out", "Location::::  " + txt);
//			Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
//		mHandler.post(new MakeToast(trackLocation(location)));
		Log.w(DEBUG_TAG, ">>>onLocationChanged: " + location.getLatitude());
		mServiceHandler.post(new MakeToast(trackLocation(location)));
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.w(DEBUG_TAG, ">>>provider disabled: " + provider);
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.w(DEBUG_TAG, ">>>provider enabled: " + provider);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.w(DEBUG_TAG, ">>>provider status changed: " + provider);
	}
}