package com.bv.teensafechild.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


import com.bv.teensafechild.constants.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashSet;
import java.util.Set;

@SuppressLint("NewApi")
public class Pref {

	private static SharedPreferences sharedPreferences = null;
	public static String KEY_ALL = "all_monitor";
	public static String KEY_STATUS_BROWSER = "status";
	public static String KEY_STATUS_CONTACT = "contact";
	public static String KEY_STATUS_LOG = "log";
	public static String KEY_STATUS_APP = "appinstall";

	public static String KEY_STATUS_GPSLOC = "gps_loc";
	public static String KEY_STATUS_MSGLOG = "msg_log";

	public static void openPref(Context context) {

		sharedPreferences = context.getSharedPreferences(Constants.PREF_FILE,
				Context.MODE_PRIVATE);

	}

	public static String getValue(Context context, String key,
			String defaultValue) {
		Pref.openPref(context);
		String result = Pref.sharedPreferences.getString(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, String value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putString(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

	public static boolean getValue(Context context, String key,
			boolean defaultValue) {
		Pref.openPref(context);
		boolean result = Pref.sharedPreferences.getBoolean(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, boolean value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putBoolean(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static int getValue(Context context, String key,
			int defaultValue) {
		Pref.openPref(context);
		int result = Pref.sharedPreferences.getInt(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, int value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putInt(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static void setStringSet(Context _Context, String key,
			Set<String> mSetArray) {

		Pref.openPref(_Context);

		Editor preferenceEditor = Pref.sharedPreferences.edit();
		preferenceEditor.putStringSet(key, mSetArray);
		preferenceEditor.commit();
		preferenceEditor = null;
		Pref.sharedPreferences = null;
	}

	public static Set<String> getStoredPassHistory(Context _Context, String mKey) {

		Pref.openPref(_Context);

		HashSet<String> mSetPassHistory = (HashSet<String>) Pref.sharedPreferences
				.getStringSet(mKey, new HashSet<String>());

		Pref.sharedPreferences = null;

		return mSetPassHistory;
	}
	public static Editor getPrefEditor(Context mContext){
		Pref.openPref(mContext);
		
		Editor mEditor = Pref.sharedPreferences.edit();
		return mEditor;
	}
	public static void clearData(Context context, String key){
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.remove(key);
		prefsPrivateEditor.commit();
	}
	public static LatLng getValue(Context context, String key) {
		Pref.openPref(context);
		double latitude = 0;
		double longitude = 0;
		try {
			latitude = Double.parseDouble(Pref.sharedPreferences.getString(key+"_lat", "0"));
			longitude = Double.parseDouble(Pref.sharedPreferences.getString(key+"_long", "0"));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LatLng result = new LatLng(latitude, longitude);

		Pref.sharedPreferences = null;
		return result;
	}
	public static void setValue(Context context, String key, LatLng value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();

		String latitude = ""+value.latitude;
		String longitude = ""+value.longitude;

		prefsPrivateEditor.putString(key+"_lat", latitude);
		prefsPrivateEditor.putString(key+"_long", longitude);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

}
