package com.bv.teensafechild.util;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Patterns;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bv.teensafechild.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@SuppressLint("NewApi")
public class Utils {
    public final static int DEFAULT_DELAY = 0;

	private static Dialog dialog = null;

	public final static int COLOR_ANIMATION_DURATION = 1000;
	public static int[][] states = new int[][]{
			new int[]{android.R.attr.state_enabled}, // enabled
			new int[]{-android.R.attr.state_enabled}, // disabled
			new int[]{-android.R.attr.state_checked}, // unchecked
			new int[]{android.R.attr.state_pressed}  // pressed
	};
	Context mContext;
	private static final int b = 360;
	/**
	 * Regular Expression to match the US Zip-Code
	 */
	static final String regex = "^\\d{5}(\\d{4})?$";
	static final String passregex= "^(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@!%*?&\\-+_()]{8,}$";

	public Utils(Context mContext) {
		this.mContext = mContext;
	}

	/*
	 * set ListView height based on height of childen
	 */
	public static void setListViewHeightBasedOnChildren(GridView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
		}
		LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ( (listAdapter.getCount()));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	/**
	 * @author ubbvand2 Himanshu Method use to load Photo of Contact it will
	 *         load Photo Either from Contact _Id or from PhotoId
	 * @param cr
	 * @param id
	 * @param photo_id
	 * @return
	 */
	public static Bitmap loadContactPhoto(ContentResolver cr, long id,
			long photo_id) {

		Uri uri = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, id);
		InputStream input = ContactsContract.Contacts
				.openContactPhotoInputStream(cr, uri);
		if (input != null) {
			return BitmapFactory.decodeStream(input);
		} else {
			// Log.d("PHOTO","first try failed to load photo");

		}

		byte[] photoBytes = null;

		Uri photoUri = ContentUris.withAppendedId(
				ContactsContract.Data.CONTENT_URI, photo_id);

		Cursor c = cr.query(photoUri,
				new String[] { ContactsContract.CommonDataKinds.Photo.PHOTO },
				null, null, null);

		try {
			if (c.moveToFirst())
				photoBytes = c.getBlob(0);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		} finally {

			c.close();
		}

		if (photoBytes != null)
			return BitmapFactory.decodeByteArray(photoBytes, 0,
					photoBytes.length);
		else
			// Log.d("PHOTO","second try also failed");
			return null;
	}

	public static Animation expand(final View v, final boolean expand) {
		try {
			Method m = v.getClass().getDeclaredMethod("onMeasure", int.class,
					int.class);
			m.setAccessible(true);
			m.invoke(v,
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
					MeasureSpec.makeMeasureSpec(
							((View) v.getParent()).getMeasuredWidth(),
							MeasureSpec.AT_MOST));
		} catch (Exception e) {
			e.printStackTrace();
		}

		final int initialHeight = v.getMeasuredHeight();

		if (expand) {
			v.getLayoutParams().height = 0;
			v.setVisibility(View.GONE);
		} else {
			v.getLayoutParams().height = initialHeight;
			v.setVisibility(View.VISIBLE);
		}

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				int newHeight = 0;

				if (expand) {
					newHeight = (int) (initialHeight * interpolatedTime);
				} else {
					newHeight = (int) (initialHeight * (1 - interpolatedTime));
				}

				v.getLayoutParams().height = newHeight;
				v.requestLayout();

				if (interpolatedTime == 1 && !expand)
					v.setVisibility(View.GONE);
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};
		a.setDuration((int) (initialHeight / v.getContext().getResources()
				.getDisplayMetrics().density));// SPEED_ANIMATION_TRANSITION

		return a;
	}

	public static boolean isNotNullOrBlank(String mString) {
		if (mString==null){
			return false;
		}else if (mString.equalsIgnoreCase("")) {
			return false;
		}else if (mString.equalsIgnoreCase("null")) {
			return false;
		} else if (mString.equalsIgnoreCase(null)) {
			return false;
		} else {
			return true;
		}
	}

	public interface CustomAlertDialogControles {
		public void onOkButtonClickListener();
	}
	
	public interface CustomAlertDialogTwoButtons{
		public void leftButtonClickEvent();
		public void rightButtonClickEvent();
		
	}
	/*for open other application*/
	public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
		if (i == null) {
		    return false;
		    //throw new PackageManager.NameNotFoundException();
		}
		i.addCategory(Intent.CATEGORY_LAUNCHER);
		context.startActivity(i);
		return true;
    }

	/**
	 * Set Ripple effect to view
	 * param mContext: context of screen
	 * param view: View
	 */
	public static void setRippleEffect(Context mContext, View view) {
		int[] colors = new int[]{
				mContext.getResources().getColor(R.color.colorAccent),
				mContext.getResources().getColor(R.color.colorAccent),
				Color.GREEN,
				Color.WHITE
		};
		ColorStateList myList = new ColorStateList(Utils.states, colors);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			/*if (view instanceof AppCompatButton){
				((AppCompatButton)view).setSupportBackgroundTintList(myList);
			}else if (view instanceof AppCompatButton){
				((FloatingActionButton)view).setRippleColor(mContext.getResources().getColor(R.color.colorAccent));
			}else{
				ViewCompat.setBackgroundTintList(view, myList);
				AlphaAnimation obja = new AlphaAnimation(1.0f, 0.3f);
				obja.setDuration(5);
				obja.setFillAfter(false);
			}*/
		} else {
			ViewCompat.setBackgroundTintList(view, myList);
			AlphaAnimation obja = new AlphaAnimation(1.0f, 0.3f);
			obja.setDuration(5);
			obja.setFillAfter(false);
		}
	}
	/**
	 * Set Ripple effect to view
	 * param mContext: context of screen
	 * param view: View
	 */
	/*public static void setRippleEffect(Context mContext, android.support.design.widget.FloatingActionButton view) {
		int[] colors = new int[]{
				mContext.getResources().getColor(R.color.colorPrimary),
				mContext.getResources().getColor(R.color.colorPrimary),
				mContext.getResources().getColor(R.color.colorPrimaryDark),
				Color.WHITE
		};
		ColorStateList myList = new ColorStateList(Utils.states, colors);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			view.setBackgroundTintList(myList);
		} else {
			view.setBackgroundTintList(myList);
			ViewCompat.setBackgroundTintList(view, myList);
		}
	}*/

	/**
	 * Show Dialog During web service calling
	 */
	public void showProgressBar() {
		dialog = new Dialog(mContext, android.R.style.Theme_Translucent);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.setCancelable(false);

		/*Add any custom layout as per your requirement and uncomment below code*/

		/*LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewChild = inflater.inflate(R.layout.loader, null);

		dialog.setContentView(viewChild);*/

		Runtime.getRuntime().gc();
		System.gc();

		try {
			dialog.show();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

//	currentTime = Utils.getSeconds("dd-MM-yyyy HH:mm:ss", Utils.getCurrentDate("dd-MM-yyyy HH:mm:ss"));


	/**
	 * Hide progress dialog
	 */
	public void hideProgressBar() {
		if (dialog != null && dialog.isShowing())
			dialog.dismiss();
	}

	/**
	 * Change the color of a view with an animation
	 *
	 * @param v          the view to change the color
	 * @param startColor the color to start animation
	 * @param endColor   the color to end the animation
	 */
	public static void animateViewColor(View v, int startColor, int endColor) {

		ObjectAnimator animator = ObjectAnimator.ofObject(v, "backgroundColor",
				new ArgbEvaluator(), startColor, endColor);

//        animator.setInterpolator(new PathInterpolator(0.4f,0f,1f,1f));
		animator.setDuration(COLOR_ANIMATION_DURATION);
		animator.start();
	}
    /* Check Internet Connectivity */

	public static boolean isOnline(Context context) {

		try {
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			if (cm != null) {
				return cm.getActiveNetworkInfo().isConnectedOrConnecting();
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static void showToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public static void showToastLong(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	/*public static void showSnackbar(Context context, String message, View view) {
		Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
		*//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){

		}*//*
		View snackbarView = snackbar.getView();
		TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
		textView.setMaxLines(4);  // show multiple line
		snackbar.show();
	}

	public static void showSnackbarLong(Context context, String message, View view) {
		Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
	}
*/
	@SuppressLint("NewApi")
	public static int getDeviceWidth(Context mContext) {
		Display display = ((WindowManager) mContext
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		return width;
	}

	@SuppressLint("NewApi")
	public static int getDeviceHeight(Context mContext) {
		Display display = ((WindowManager) mContext
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int height = size.y;
		return height;
	}


	public static Bitmap getCircularBitmap(Bitmap bitmap) {
		Bitmap output;

		if (bitmap.getWidth() > bitmap.getHeight()) {
			output = Bitmap.createBitmap(bitmap.getHeight(),
					bitmap.getHeight(), Config.ARGB_8888);
		} else {
			output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(),
					Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		float r = 0;

		if (bitmap.getWidth() > bitmap.getHeight()) {
			r = bitmap.getHeight() / 2;
		} else {
			r = bitmap.getWidth() / 2;
		}

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(r, r, r, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	public static Bitmap cropImage(Bitmap bitMap, int Width, int Height) {
		int bitWidth = bitMap.getWidth();
		int bitHeight = bitMap.getHeight();

		int X = 0;
		int Y = 0;

		if (bitHeight < bitWidth) {
			X = (bitWidth / 2) - (Width / 2);
		} else {
			Y = (bitHeight / 2) - (Height / 2);
		}

		if ((X + Width) <= bitWidth && (Y + Height) <= bitHeight)
			return Bitmap.createBitmap(bitMap, X, Y, Width, Height);
		else
			return bitMap;
	}

	public static Bitmap resize(Bitmap bitMap, int width, int height) {

		int per;
		int bitWidth = bitMap.getWidth();
		int bitHeight = bitMap.getHeight();

		if (bitHeight < bitWidth) {
			per = (height * 100) / bitHeight;
			bitHeight = height;
			bitWidth = (bitWidth * per) / 100;
		} else {
			per = (width * 100) / bitWidth;
			bitWidth = width;
			bitHeight = (bitHeight * per) / 100;
		}
		return Bitmap.createScaledBitmap(bitMap, bitWidth, bitHeight, false);
	}

	/**
	 * method is used for checking valid email id format.
	 *
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/* Return LastSeen with Date&Time */

	public static String getLastSeenText(long last_seen) {

		String LAST_SEEN_STRING = "";

		long todays_start_millis = Utils.convertStringToDate(
				Utils.convertDateToString(new Date(), "dd/MM/yyyy"),
				"dd/MM/yyyy").getTime();

		long todays_end_millis = todays_start_millis + 1000 * 60 * 60 * 24;

		long yesterday_start_millis = todays_start_millis - 1000 * 60 * 60 * 24;

		long yesterday_end_millis = todays_start_millis - 1000;

		long currMillis = new Date().getTime();

		if (last_seen < currMillis + 1000 * 30
				&& last_seen > currMillis - 1000 * 30) {
			LAST_SEEN_STRING = "Online";
		} else if (last_seen > todays_start_millis
				&& last_seen < todays_end_millis) {
			LAST_SEEN_STRING = "Last Seen Today at "
					+ Utils.millisToDate(last_seen, "hh:mm aa");
		} else if (last_seen > yesterday_start_millis
				&& last_seen < yesterday_end_millis) {
			LAST_SEEN_STRING = "Last Seen Yesterday at "
					+ Utils.millisToDate(last_seen, "hh:mm aa");
		} else {
			LAST_SEEN_STRING = "Last Seen at "
					+ Utils.millisToDate(last_seen, "dd/MM/yyyy hh:mm aa");
		}
		Log.print("LAST_SEEN_STRING  :: " + LAST_SEEN_STRING);
		return LAST_SEEN_STRING;
	}

	/* Date To Millis */

	public static long dateToMillis(String dateString, String format) {
		long timeInMilliseconds = 0;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			Date mDate = sdf.parse(dateString);
			timeInMilliseconds = mDate.getTime();
			Log.print("Date in milli :: " + timeInMilliseconds);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return timeInMilliseconds;
	}

	/* Millis To Date */

	public static String millisToDate(long millis, String format) {

		return new SimpleDateFormat(format).format(new Date(millis));
	}

	/* Date to String */

	public static String convertDateToString(Date objDate, String parseFormat) {
		try {
			return new SimpleDateFormat(parseFormat).format(objDate);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/* String to Date */

	public static Date convertStringToDate(String strDate, String parseFormat) {
		try {
			return new SimpleDateFormat(parseFormat).parse(strDate);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/* Milliseconds to date in String */

	public static String getDate(long milliSeconds, String format) {
		// Create a DateFormatter object for displaying date in specified
		// format.
		Date date = new Date(milliSeconds);
		SimpleDateFormat dateformat = new SimpleDateFormat(format);
		System.out.println(dateformat.format(date));
		return dateformat.format(date);
	}


	private static int getTimeDistanceInMinutes(long time) {
		long timeDistance = new Date().getTime() - time;
		return Math.round((Math.abs(timeDistance) / 1000) / 60);
	}

	/* Hide/Close SoftKeyboard */

	public static void hideSoftKeyboard(Activity activity) {

		/*InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
				.getWindowToken(), 0);*/

		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	/* Convert Dp to Pixel */

	public static int convertDpToPx(Context context, float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	public static int convertPxToDp(Context context, float px) {
		final int scale = (int) (px / context.getResources()
				.getDisplayMetrics().density);


		return scale;
	}

	/**
	 * Copies text into Clip Board
	 *
	 * @param mContext
	 * @param mText
	 */
	public static void copyTextToClipBoard(Context mContext, String mText) {
		ClipboardManager myClipboard = (ClipboardManager) mContext
				.getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData myClip = ClipData.newPlainText(mContext.getResources().getString(R.string.app_name), mText);
		myClipboard.setPrimaryClip(myClip);
	}

	/*
     * set ListView height based on height of childen
     */
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
			Log.print("" + totalHeight);
		}

		LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	/**
	 * Returns MD5 of a string
	 */
	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			// Now we need to zero pad it if you actually want the full 32
			// chars.
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static void showAlertDialog(Context context, String mMsgString) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
//		builder.setTitle(context.getResources().getString(R.string.app_name));
		builder.setMessage(mMsgString);
		builder.setPositiveButton("OK", null);
		builder.setNegativeButton("Cancel", null);
		builder.show();

	}

	public static void showErrorAlertDialog(final Context context, String mMsgString, final View view) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
		builder.setTitle(context.getResources().getString(R.string.app_name));
		builder.setMessage(mMsgString);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (view != null) {
					view.requestFocus();
					showKeyboard(context, view);
				}

			}
		});
		builder.show();

	}

	public static void setEnlargeHitArea(final Button mButton) {
		try {
			final View parent = (View) mButton.getParent();  // button: the view you want to enlarge hit area
			parent.post(new Runnable() {
				public void run() {
					final Rect rect = new Rect();
					mButton.getHitRect(rect);
					rect.top -= 100;    // increase top hit area
					rect.left -= 100;   // increase left hit area
					rect.bottom += 100; // increase bottom hit area
					rect.right += 100;  // increase right hit area
					parent.setTouchDelegate(new TouchDelegate(rect, mButton));
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setEnlargeHitArea(final TextView mButton) {
		try {
			final View parent = (View) mButton.getParent();  // button: the view you want to enlarge hit area
			parent.post(new Runnable() {
				public void run() {
					final Rect rect = new Rect();
					mButton.getHitRect(rect);
					rect.top -= 50;    // increase top hit area
					rect.left -= 150;   // increase left hit area
					rect.bottom += 100; // increase bottom hit area
					rect.right += 150;  // increase right hit area
					parent.setTouchDelegate(new TouchDelegate(rect, mButton));
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setEnlargeHitArea(final ImageView mButton) {
		try {
			final View parent = (View) mButton.getParent();  // button: the view you want to enlarge hit area
			parent.post(new Runnable() {
				public void run() {
					final Rect rect = new Rect();
					mButton.getHitRect(rect);
					rect.top -= 100;    // increase top hit area
					rect.left -= 100;   // increase left hit area
					rect.bottom += 100; // increase bottom hit area
					rect.right += 100;  // increase right hit area
					parent.setTouchDelegate(new TouchDelegate(rect, mButton));
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String convertDateFormate(String dateString, String inputDateString, String outputDateString) {
		Date date;
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat(inputDateString);
		try {
			date = dateFormatLocal.parse(dateString);
			return new SimpleDateFormat(outputDateString).format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "";
		}
	}

	public static boolean isNotNull(String mString) {
		if (mString == null) {
			return false;
		} else if (mString.equalsIgnoreCase("")) {
			return false;
		} else if (mString.equalsIgnoreCase("N/A")) {
			return false;
		} else if (mString.equalsIgnoreCase("[]")) {
			return false;
		} else if (mString.equalsIgnoreCase("null")) {
			return false;
		}else if (mString.equalsIgnoreCase("{}")) {
			return false;
		}
		else {
			return true;
		}
	}

	public static void expand(final View v) {

		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final int measuredHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1
						? LayoutParams.WRAP_CONTENT
						: (int) (measuredHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp per milliseconds
		a.setDuration((int) (measuredHeight / v.getContext().getResources().getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp per milliseconds
		a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static void setTextOnly(EditText mEditText) {
		mEditText.setFilters(new InputFilter[]{
				new InputFilter() {
					public CharSequence filter(CharSequence src, int start,
											   int end, Spanned dst, int dstart, int dend) {
						if (src.equals("")) { // for backspace
							return src;
						}
						if (src.toString().matches("[a-zA-Z ]+")) {
							return src;
						}
						return "";
					}
				}
		});
	}

	public static String getCurrentDate() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}
	/**
	 * Method to get Current day Date
	 *
	 * @param dateFormat
	 * @return
	 */
	public static String getCurrentDate(String dateFormat) {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df;
		if (isNotNull(dateFormat))
			df = new SimpleDateFormat(dateFormat);
		else
			df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Hide keyboard if user touch outside editText..
	 * param view: parent view
	 * param mContext: context
	 */
	public static void setupOutSideTouchHideKeyboard(final View view, final Context mContext) {

		//Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					InputMethodManager mgr = (InputMethodManager)
							mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
					return false;
				}

			});
		}

		//If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupOutSideTouchHideKeyboard(innerView, mContext);
			}
		}
	}

	public static void showKeyboard(Context context, View v) {
		/*InputMethodManager mgr = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		mgr.showSoftInput(v, 0);*/

		InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.toggleSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);

	}

	static int dpToPx(Context context, float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return Math.round(dp * scale);
	}

	static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	static boolean hasLollipop() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
	}


	public static TextView getEmptyTextView(Context _Context) {
		TextView mTxtView = new TextView(_Context);
		mTxtView.setTextSize(16);
		mTxtView.setText("No records found");
		mTxtView.setGravity(Gravity.CLIP_HORIZONTAL);
		//mTxtView.setTextColor(_Context.getResources().getColor(R.color.colorPrimary, null));

		return mTxtView;
	}


	/**
	 * for Android M+
	 * check whether permission is enable or not
	 */
	/*public static boolean checkNeedsPermission(Context mContext, String permission) {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED;
	}*/


	public static float getDistance(LatLng start_latlong, LatLng end_latlong) {
		Location l1 = new Location("One");
		l1.setLatitude(start_latlong.latitude);
		l1.setLongitude(start_latlong.longitude);

		Location l2 = new Location("Two");
		l2.setLatitude(end_latlong.latitude);
		l2.setLongitude(end_latlong.longitude);

		float distance = l1.distanceTo(l2);
//		android.util.Log.d("System out", "Distance in M:: "+distance);
//		if (distance > 1000.0f) {
//		distance = distance / 1000.0f;
		android.util.Log.e("System out", "Distance in Meters:: "+distance);
//		}
		return distance;
	}

	public static String replaceString(String text)
	{

		if(text.contains("&amp;"))
			text = text.replace("&amp;", "&");
		if(text.contains("&quot;"))
			text = text.replace("&quot;","\"");
		if(text.contains("&#39;"))
			text = text.replace("&#39;", "'");
		if(text.contains("&#180;"))
			text = text.replace("&#180;", "'");
//		if(text.contains("’"))
//			text = text.replace("’", "'");
		if(text.contains("&gt;"))
			text = text.replace("&gt;", ">");
		if(text.contains("<br>"))
			text = text.replace("<br>", "\n");
		if(text.contains("&lt;"))
			text =text.replace("&lt;", "<");
		if(text.contains("&nbsp;"))
			text =text.replace("&nbsp;", " ");
		if(text.contains("\\/"))
			text =text.replace("\\/", "/");
//		if(text.contains("\n"))
//			text =text.replace("\n", "<br>");
		if(text.contains("\r\n"))
			text =text.replace("\r\n", "<br/>");
        /*if(text.contains("\n"))
            text = text.replace("\n", "<br/>");*/
		/*if(text.contains(""))
			text = text.replace("\"", "");*/

		return text;
	}

	public static String getRealPathFromURI(final Context context, final Uri uri) {

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

		// DocumentProvider
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/"
							+ split[1];
				}

				// TODO handle non-primary volumes
			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"),
						Long.valueOf(id));

				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] { split[1] };

				return getDataColumn(context, contentUri, selection,
						selectionArgs);
			}
		}
		// MediaStore (and general)
		else if ("content".equalsIgnoreCase(uri.getScheme())) {
			return getDataColumn(context, uri, null, null);
		}
		// File
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}
	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri
				.getAuthority());
	}
	public static String getDataColumn(Context context, Uri uri,
									   String selection, String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = { column };

		try {
			cursor = context.getContentResolver().query(uri, projection,
					selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(column_index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	// Compress image
	public String compressImage(String imageUri) {

		String filePath = imageUri;
		Bitmap scaledBitmap = null;

		BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
		options.inJustDecodeBounds = true;
		Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

		int actualHeight = options.outHeight;
		int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

		float maxHeight = 816.0f;
		float maxWidth = 612.0f;
		float imgRatio = actualWidth / actualHeight;
		float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

		if (actualHeight > maxHeight || actualWidth > maxWidth) {
			if (imgRatio < maxRatio) {
				imgRatio = maxHeight / actualHeight;
				actualWidth = (int) (imgRatio * actualWidth);
				actualHeight = (int) maxHeight;
			} else if (imgRatio > maxRatio) {
				imgRatio = maxWidth / actualWidth;
				actualHeight = (int) (imgRatio * actualHeight);
				actualWidth = (int) maxWidth;
			} else {
				actualHeight = (int) maxHeight;
				actualWidth = (int) maxWidth;

			}
		}

//      setting inSampleSize value allows to load a scaled down version of the original image

		options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
		options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inTempStorage = new byte[16 * 1024];

		try {
//          load the bitmap from its path
			bmp = BitmapFactory.decodeFile(filePath, options);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();

		}
		try {
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Config.ARGB_8888);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();
		}

		float ratioX = actualWidth / (float) options.outWidth;
		float ratioY = actualHeight / (float) options.outHeight;
		float middleX = actualWidth / 2.0f;
		float middleY = actualHeight / 2.0f;

		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

		Canvas canvas = new Canvas(scaledBitmap);
		canvas.setMatrix(scaleMatrix);
		canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
		ExifInterface exif;
		try {
			exif = new ExifInterface(filePath);

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION, 0);
			Log.debug("EXIF", "Exif: " + orientation);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
				Log.debug("EXIF", "Exif: " + orientation);
			} else if (orientation == 3) {
				matrix.postRotate(180);
				Log.debug("EXIF", "Exif: " + orientation);
			} else if (orientation == 8) {
				matrix.postRotate(270);
				Log.debug("EXIF", "Exif: " + orientation);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
					scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
					true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileOutputStream out = null;
		String filename =  imageUri.substring(imageUri.lastIndexOf("/"), imageUri.length());
//        getFilename();
		try {
			out = new FileOutputStream(imageUri);

//          write the compressed bitmap at the destination specified by filename.
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return filename;

	}

	// calculate inSample size
	public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		final float totalPixels = width * height;
		final float totalReqPixelsCap = reqWidth * reqHeight * 2;
		while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
			inSampleSize++;
		}

		return inSampleSize;
	}

	public static boolean isValidUrl (String txtWebsite){
		String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
//        String pattern="(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$";
		if(Patterns.WEB_URL.matcher(txtWebsite).matches())
		{
			return true;
		}else{
			return false;
		}
	}

	public String getDeviceFullInfo(){
		StringBuilder errorReport=new StringBuilder();
		errorReport.append("\n************ DEVICE INFORMATION ***********\n");
		errorReport.append("Brand: ");
		errorReport.append(Build.BRAND);
		errorReport.append(" Device: ");
		errorReport.append(Build.DEVICE);
		errorReport.append(" Model: ");
		errorReport.append(Build.MODEL);
		errorReport.append("\n************ FIRMWARE ************\n");
		errorReport.append(" SDK: ");
		errorReport.append(Build.VERSION.SDK_INT);
		errorReport.append(" SDK NAME: ");
		Field[] fields = Build.VERSION_CODES.class.getFields();
		for (Field field : fields) {
			String fieldName = field.getName();
			int fieldValue = -1;

			try {
				fieldValue = field.getInt(new Object());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			if (fieldValue == Build.VERSION.SDK_INT) {
				errorReport.append(fieldName);
			}
		}

		errorReport.append("Release: ");
		errorReport.append(Build.VERSION.RELEASE);
		errorReport.append("Incremental: ");
		errorReport.append(Build.VERSION.INCREMENTAL);
		Log.print("------------ "+errorReport.toString());

		return errorReport.toString();
	}
	public static Intent newEmailIntent(Context context, String address,
										String subject, String body, String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}

	public static int convertToDensityPixels(Context context, int densityPixels) {
		float scale = context.getResources().getDisplayMetrics().density;
		int pixels = (int) (densityPixels * scale + 0.5f);
		return pixels;
	}

	public static String getDomainName(String url) {
		URI uri;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			return url;
		}
		String domain = uri.getHost();
		if (domain == null) {
			return url;
		}
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}
	public static String capitlizeFirstChar(String string) {
		String s = "";
		try {
			s = Character.toUpperCase(string.charAt(0)) + string.substring(1);
		} catch (Exception e) {
			e.printStackTrace();
			s = string;
		}
		return s;
	}

	public static String camelCaseToUnderscodes(String string) {
		return string.replaceAll("([a-z])([A-Z])", "$1_$2").toLowerCase();
	}

	public static String camelCaseTo2Words(String string) {
		return capitlizeFirstChar(string).replaceAll("([a-z])([A-Z])", "$1 $2");
	}

	public static String concat(Collection<String> strings, String sep) {
		String str = null;
		if (!(strings == null || strings.size() == 0)) {
			boolean first = true;
			str = " ";
			for (String str2 : strings) {
				if (first) {
					str = str + str2;
					first = false;
				} else {
					str = str + sep + str2;
				}
			}
		}
		return str;
	}

	public static String concat(String[] strings, String sep) {
		if (strings == null || strings.length == 0) {
			return null;
		}
		String ret = " ";
		for (int i = 0; i < strings.length - 1; i++) {
			ret = ret + strings[i] + sep;
		}
		return ret + strings[strings.length - 1];
	}

	public static String capitalize(String word) {
		if (word == null) {
			return null;
		}
		if (word.length() == 0) {
			return word;
		}
		String firstLetter = word.substring(0, 1).toUpperCase();
		return firstLetter + word.substring(1);
	}
	public static boolean isNumeric(String s) {
		return s.matches("[-+]?\\d*\\.?\\d+");
	}

	public static String printFbHash(Context c) {
		try {
			Signature[] arr$ = c.getPackageManager().getPackageInfo("com.androidapparchitecture", PackageManager.GET_SIGNATURES).signatures;
			if (0 < arr$.length) {
				Signature signature = arr$[0];
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String encodeToString = Base64.encodeToString(md.digest(), 0);
				android.util.Log.d("KeyHash:", encodeToString);
				return encodeToString;
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e2) {
			e2.printStackTrace();
		}
		return null;
	}

	public static Bitmap roundedImage(Bitmap bitmap) {
		Bitmap a = bitmap;
		Bitmap createBitmap = Bitmap.createBitmap(a.getWidth(), a.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(createBitmap);
		Paint paint = new Paint();
		int min = Math.min(a.getWidth(), a.getHeight());
		Rect rect = new Rect(0, 0, min, min);
		RectF rectF = new RectF(rect);
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(Color.BLUE);
		canvas.drawOval(rectF, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(a, rect, rect, paint);
		a.recycle();
		return createBitmap;
	}

	public static Bitmap b(Bitmap bitmap) {
		int i;
		int i2 = 0;
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int i3 = height > width ? width : height;
		if (height > width) {
			i = height - (height - width);
		} else {
			i = height;
		}
		int i4 = (width - height) / 2;
		if (i4 < 0) {
			i4 = 0;
		}
		height = (height - width) / 2;
		if (height >= 0) {
			i2 = height;
		}
		return Bitmap.createBitmap(bitmap, i4, i2, i3, i);
	}
	public static void goBack(FragmentManager mManager, FragmentActivity activity){
		if (mManager.getBackStackEntryCount() == 1) {
			activity.finish();
		} else {
			mManager.popBackStack();
		}
	}
	public static void setUnderlineTV(TextView textView){
		textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
	}

	/**
	 * This method returns true if the parameter string is a valid zip code
	 */
	public static boolean isAValidZipCode(String zip) {
		return Pattern.matches(regex, zip);
	}

	/**
	 * This method returns true if the parameter string is a valid password
	 */
	public static boolean isAValidPassword(String password) {
		return Pattern.matches(passregex, password);
	}

	public static boolean isServiceRunning(Class<?> serviceClass,Context con) {
		ActivityManager manager = (ActivityManager) con.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
/*public static String getDuration(String startTime,String endTime){

	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date1 = null;
	Date date2 = null;
	try {
		date1 = format.parse(startTime);
		date2 = format.parse(endTime);
	} catch (ParseException e) {
		e.printStackTrace();
	}

	long difference = date2.getTime() - date1.getTime();
	// Create a calendar object that will convert the date and time value in milliseconds to date.
	Calendar calendar = Calendar.getInstance();
	calendar.setTimeInMillis(difference);
	SimpleDateFormat formatedTime = new SimpleDateFormat("HH:mm:ss");
	String formattedDate = formatedTime.format(calendar.getTime());
	return formattedDate;
}*/
	/***
	 * @param totalSeconds : Seconds
	 * @return : time in hour:minute:second format
	 */
	public static String timeFormat(int totalSeconds) {
		int seconds = totalSeconds % 60;
		int minutes = (totalSeconds / 60) % 60;
		int hours = totalSeconds / 3600;

		String strHours, strMinutes, strSeconds, strToReturn;
		if (hours > 0)
			strHours = String.valueOf(hours);
		else
			strHours = "00";
		if (minutes > 0)
			strMinutes = String.valueOf(minutes);
		else
			strMinutes = "00";
		if (seconds > 0)
			strSeconds = String.valueOf(seconds);
		else
			strSeconds = "00";

		if (hours > 0) {
			strToReturn = String.valueOf(strHours + ":" + strMinutes + ":" + strSeconds);
		} else {
			strToReturn = String.valueOf("00:" + strMinutes + ":" + strSeconds);
		}
		return strToReturn;
	}
	/**
	 * @param inputFormat:  date format
	 * @param dateInString: date in string format
	 * @return seconds from date
	 */
	public static long getSeconds(String inputFormat, String dateInString) {
		SimpleDateFormat sdf = new SimpleDateFormat(inputFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
		Date date = null;
		try {
			date = sdf.parse(dateInString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long timeInMilliSecond = date.getTime();
		return (long) (timeInMilliSecond / 1000.0); // milliseconds to seconds
	}
}
